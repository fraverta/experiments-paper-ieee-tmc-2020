from brufn.contact_plan import ContactPlan
from brufn.brufspark import BRUFSpark
from brufn.network import Net
from brufn.experiment_generator import *
from brufn.utils import print_str_to_file
from brufn.helper_cgr_funciton_generator import OneCopyHelperFunctionGenerator
from brufn.helper_ibrufn_function_generator6 import IBRUFNFunctionGenerator
from datetime import date
import os
import simplejson as json
from pyspark import SparkContext, SparkConf
from brufn.net_metrics_generator import NetMetricGenerator
import simplejson as json
from brufn.helper_ibrufn_function_generator6 import IBRUFNFunctionGenerator


OUTPUT_PATH = 'exp-24-06-2020' #f'exp-{date.today().strftime("%d-%m-%Y")}' #'exp-05-10-2019'
CP_PATH = "10NetICC"
DTNSIM_PATH = '/home/fraverta/development/dtnsim/dtnsim/src'
ALGORITHMS = [CGR_FA, CGR_2COPIES, CGR_BRUF_POWERED, CGR_MODEL350, CGR_HOPS, BRUF_4, BRUF_3, BRUF_2, BRUF_1, IBRUF_4, IBRUF_3, IBRUF_2, IBRUF_1, SPRAY_AND_WAIT_4, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_2]
NUM_OF_REPS = 100
TS_DURATION_IN_SECONDS = 10

def compute_bruf(algorithm, net, sources, targets, bruf_dir, rc):
    copies = int(algorithm[algorithm.index('-') + 1:])
    bruf_result_dir = os.path.join(bruf_dir, 'states_files'); os.makedirs(bruf_result_dir, exist_ok=True)
    routing_files_path = os.path.join(bruf_dir, 'routing_files'); os.makedirs(routing_files_path, exist_ok=True)

    os.makedirs(os.path.join(bruf_dir, 'routing_files'), exist_ok=True)
    for target in targets:
        bruf_result_dir_by_target = os.path.join(bruf_result_dir, f'to-{target}'); os.makedirs(bruf_result_dir_by_target,exist_ok=True)
        valid_sources = [s for s in sources if s != target]
        bruf = BRUFSpark(net, valid_sources, target, copies, [i / 100 for i in range(0, 110, 10)], bruf_result_dir_by_target)
        conf = SparkConf().setAppName("BRUF-Spark")
        conf = (conf.setMaster('local[2]')
                .set('spark.executor.memory', '3G')
                .set('spark.driver.memory', '4G')
                .set('spark.driver.maxResultSize', '4G'))
        sc = SparkContext(conf=conf)
        bruf.compute_bruf(sc, num_states_per_slice=100, reachability_closure=rc)
        sc.stop()
        bruf.generate_mc_to_dtnsim_all_sources_all_pf(routing_files_path, partially_save_ts_number=10)

def generate_bruf_function(algorithm, net, sources, targets, bruf_dir):
    bruf_result_dir = os.path.join(bruf_dir, 'states_files'); os.makedirs(bruf_result_dir, exist_ok=True)
    routing_files_path = os.path.join(bruf_dir, 'routing_files'); os.makedirs(routing_files_path, exist_ok=True)

    os.makedirs(os.path.join(bruf_dir, 'routing_files'), exist_ok=True)
    for target in targets:
        valid_sources = [s for s in sources if s != target]
        bruf = BRUFSpark(net, valid_sources, target, 1, [i / 100 for i in range(0, 110, 10)], bruf_result_dir)
        conf = SparkConf().setAppName("BRUF-Spark")
        conf = (conf.setMaster('local[6]')
                .set('spark.executor.memory', '3G')
                .set('spark.driver.memory', '12G')
                .set('spark.driver.maxResultSize', '8G'))
        sc = SparkContext(conf=conf)
        bruf.compute_bruf(sc, num_states_per_slice=100)
        sc.stop()

        for pf in [i / 100 for i in range(0, 110, 10)]:
            pf_helper_function_file_path = os.path.join(routing_files_path, f'pf={pf:.2f}'); os.makedirs(pf_helper_function_file_path, exist_ok=True)
            try:
                helper_function = OneCopyHelperFunctionGenerator(net, target, [i for i in range(8) if i != target], net.num_of_nodes, net.num_of_ts + 1, bruf_result_dir, failure_probability=pf).generate()
            except FileNotFoundError:
                print(f"[ERROR] Not solution for {source} to {target} - {bruf_result_dir}")
                exit(1)

            for source in valid_sources:
                print_str_to_file(json.dumps(helper_function), os.path.join(pf_helper_function_file_path, f"todtnsim-{source}-{target}-{pf:.2f}.json"))

def compute_ibruf(algorithm, net, sources, targets, algorithm_dir, bruf_dir):
    copies = int(algorithm[algorithm.index('-') + 1:])
    routing_files_path = os.path.join(algorithm_dir, 'routing_files'); os.makedirs(routing_files_path, exist_ok=True)

    for target in targets:
        valid_sources = [s for s in sources if s != target]
        func = IBRUFNFunctionGenerator(net, target, copies, bruf_dir, probability_range=[i / 100 for i in range(0, 110, 10)], path_to_load_bruf=bruf_dir).generate()		
        for c in range(1, copies + 1):
            for pf in [i / 100 for i in range(0, 110, 10)]:
                pf_dir = os.path.join(routing_files_dir, f'pf={pf:.2f}');
                os.makedirs(pf_dir, exist_ok=True)
                try:
                    print_str_to_file(json.dumps(func[c][str(pf)]), os.path.join(pf_dir, f'todtnsim-{target}-{c}-{pf:.2f}.json'))
                except BaseException as e:
                    print(f"[Exception] {e}")


os.makedirs(OUTPUT_PATH, exist_ok=True)
exp_commands = ['source  ../../venv/bin/activate', 'base_dir="$PWD"']
for net_id in range(0, 10):
    net = Net.get_net_from_file(os.path.join(CP_PATH, f'net{net_id}', 'net.py'), contact_pf_required=False)
    cp_path = os.path.join(CP_PATH, f'0.2_{net_id}')
    net_dir = os.path.join(OUTPUT_PATH, f'net-{net_id}'); os.makedirs(net_dir, exist_ok=True)
    NetMetricGenerator(net, range(net.num_of_nodes), [1, 2, 3, 4], range(net.num_of_nodes), net_dir)        
    reachability_closure = json.load(open(os.path.join(net_dir,'transitive_closure.json')),
                       object_hook=lambda d: {int(k): [int(i) for i in v] if isinstance(v, list) else v for k, v in d.items()})

    for algorithm in ALGORITHMS:
        algorithm_dir = os.path.join(net_dir, algorithm); os.makedirs(algorithm_dir, exist_ok=True)
        ini_path = os.path.join(algorithm_dir, 'run.ini')
        is_runnable = True
        frouting_path = None
        if 'BRUF-' in algorithm:
            #compute_bruf(algorithm, net, range(8), range(8), algorithm_dir, reachability_closure)
            frouting_path = 'routing_files/'
        elif CGR_BRUF_POWERED in algorithm:
            generate_bruf_function(algorithm, net, range(8), range(8), algorithm_dir)
            frouting_path = 'routing_files/'
        elif 'IRUCoPn' in algorithm:
            copies = int(algorithm[algorithm.index('-') + 1:])
            frouting_path = 'routing_files/'
            routing_files_dir = os.path.join(algorithm_dir, frouting_path); os.makedirs(routing_files_dir, exist_ok=True)
            #compute_ibruf(algorithm, net, range(8), range(8), algorithm_dir, net_dir)

        traffic = dict((s, [t for t in range(1,9) if t!=s]) for s in range(1,9))
        generate_omnet_ini_file(net.num_of_nodes, traffic, algorithm, ini_path, os.path.relpath(cp_path,algorithm_dir), frouting_path=frouting_path, ts_duration=TS_DURATION_IN_SECONDS, repeats=NUM_OF_REPS)

        generate_bash_script('run.ini', os.path.join(algorithm_dir, 'run_simulation.sh'), DTNSIM_PATH)
        exp_commands.append(f'echo && echo [Running] {algorithm_dir} && echo '
                            f'&& cd {os.path.relpath(algorithm_dir,OUTPUT_PATH)} && bash run_simulation.sh && rm results/*.out && cd "$base_dir" '
                            f'&& cd ../'
                            f'&& python -OO parametric_compute_metrics.py {OUTPUT_PATH} net-{net_id} {algorithm} {NUM_OF_REPS} '
                            f'&& bash delete_results.sh {OUTPUT_PATH} net-{net_id} {algorithm}'
                            f'&& cd "$base_dir"'
                            )

with open(os.path.join(OUTPUT_PATH, 'run_experiment.sh'), 'w') as f:
    f.write('#!/bin/bash \n')
    f.write(' && \n'.join(exp_commands))
