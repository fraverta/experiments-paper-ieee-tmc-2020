import os
from brufn.utils import getListFromFile, plot_nfunctions, prom_llist, print_str_to_file, getListFromFile,plot_bar
from brufn.experiment_generator import *
from svgtogrid import combine_into_table_and_save
import sys
sys.path.insert(0,'../')
from AlgorithmsGraphStyles import ALGORITHM_LABELS, ALGORITHM_COLORS, ALGORITHM_LINE_STYLES

def compute_f_diference(f, f_reference):
    if len(f) != len(f_reference):
        print(f)
        print(f_reference)
        raise ValueError("[Error] compute_f_diference(f, f_reference): f and f_reference must have the same lenght")
    return [(f_reference[i][0], f[i][1] - f_reference[i][1]) for i in range(len(f))]


METRICS = [('EnergyEfficiency', 'Energy Efficiency'), ("deliveryRatio","Delivery Ratio"), ("appBundleReceivedDelay:mean","Mean Delay per Bundle")]
REFERENCE_ALGORITHM = CGR_MODEL350

ROUTING_ALGORITHMS = [CGR_FA, CGR_2COPIES, CGR_BRUF_POWERED, CGR_MODEL350, CGR_HOPS, BRUF_4, BRUF_3, BRUF_2, BRUF_1, IBRUF_4, IBRUF_3, IBRUF_2, IBRUF_1, SPRAY_AND_WAIT_4, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_2]
COLORS = [ALGORITHM_COLORS[ra] for ra in ROUTING_ALGORITHMS]
LABELS = [ALGORITHM_LABELS[ra] for ra in ROUTING_ALGORITHMS]

EXP_PATH = 'exp-24-06-2020'
OUTPUT_PATH = os.path.join(EXP_PATH);

graphs = []
plots_output_dir = os.path.join(OUTPUT_PATH, 'plots-bar'); os.makedirs(plots_output_dir, exist_ok=True)
for metric in METRICS:
    f_reference = getListFromFile(os.path.join(OUTPUT_PATH,'metrics_av', REFERENCE_ALGORITHM, f'METRIC={metric[0]}.txt'))[1:-1]
    f_metric_av_by_algorithm = []
    for routing_algorithm in ROUTING_ALGORITHMS:
        f_metric_by_net = []
        ra_dir = os.path.join(OUTPUT_PATH,'metrics_av', routing_algorithm)
        print(f"{ra_dir}")
        f_metric_av = getListFromFile(os.path.join(ra_dir, f'METRIC={metric[0]}.txt'))[1:-1]
        if metric in [("deliveryRatio","Delivery Ratio"), ('EnergyEfficiency', 'Energy Efficiency')]:
            f_metric_av_by_algorithm.append([(x, y*100) for x,y in compute_f_diference(f_metric_av, f_reference)])
        elif metric == ("appBundleReceivedDelay:mean","Mean Delay per Bundle"):
            f_metric_av_by_algorithm.append([(f_reference[i][0], 100 - (f_metric_av[i][1] * 100 / f_reference[i][1])) if f_reference[i][1] != 0 else (0, 0) for i in range(len(f_reference))])

    plot_name = os.path.join(plots_output_dir, metric[0])
    plot_bar(f_metric_av_by_algorithm, plot_name, LABELS, xlabel='Probability of contact failure', ylabel='% Improvement regarding CGR', ftype='png', colors=COLORS, bar_width=0.3, separation_width=0.4)
    plot_bar(f_metric_av_by_algorithm, plot_name, LABELS, xlabel='Probability of contact failure', ylabel='% Improvement regarding CGR', ftype='svg', colors=COLORS, bar_width=0.3, separation_width=0.4)
    graphs.append(plot_name + '.svg')

join_output_path = os.path.join(plots_output_dir)
os.makedirs(join_output_path, exist_ok=True)
fname = os.path.join(join_output_path, f'joined_plots')
combine_into_table_and_save(1, 3, 0, 0, graphs, fname + '.svg')
os.system('inkscape %s &'% (fname + '.svg') )
os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))


