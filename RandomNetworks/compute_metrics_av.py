import os
from brufn.utils import getListFromFile, average, print_str_to_file, prom_llist
from brufn.experiment_generator import *

EXP_PATH = 'exp-24-06-2020'
METRICS = [("appBundleReceived:count","Delivered Bundles"),("deliveryRatio","Delivery Ratio"), ("dtnBundleSentToCom:count","Transmitted bundles"), ("appBundleReceivedDelay:mean","Mean Delay per Bundle"), ("appBundleReceivedHops:mean","Mean Hops per Bundle"), ("sdrBundleStored:timeavg", "Mean Bundles in SDR"), ('EnergyEfficiency', 'Energy Efficiency')]
ROUTING_ALGORITHMS = [CGR_FA, CGR_2COPIES, CGR_BRUF_POWERED, CGR_MODEL350, CGR_HOPS, BRUF_4, BRUF_3, BRUF_2, BRUF_1, IBRUF_4, IBRUF_3, IBRUF_2, IBRUF_1, SPRAY_AND_WAIT_4, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_2]

METRIC_AV_OUTPUT_DIR = os.path.join(EXP_PATH, 'metrics_av'); os.makedirs(METRIC_AV_OUTPUT_DIR, exist_ok=True)
for metric, metric_label in METRICS:
    functions_by_alg = dict((algorithm, []) for algorithm in ROUTING_ALGORITHMS)
    for net in range(10):#for net in NET_RANGE:
        net_path = os.path.join(EXP_PATH, f'net-{net}')
        for algorithm in ROUTING_ALGORITHMS:
            alg_path = os.path.join(net_path, algorithm)
            metric_path = os.path.join(alg_path, 'metrics', f'METRIC={metric}.txt')
            functions_by_alg[algorithm].append(getListFromFile(metric_path))
    
    for algorithm in ROUTING_ALGORITHMS:
        os.makedirs(os.path.join(METRIC_AV_OUTPUT_DIR, algorithm), exist_ok=True)
        print_str_to_file(str(prom_llist(functions_by_alg[algorithm])), os.path.join(METRIC_AV_OUTPUT_DIR, algorithm, f'METRIC={metric}.txt'))

