import os
import sys
from brufn.utils import getListFromFile, plot_nfunctions
from svgtogrid import combine_into_table_and_save
from brufn.experiment_generator import *
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from AlgorithmsGraphStyles import ALGORITHM_LABELS, ALGORITHM_COLORS, ALGORITHM_LINE_STYLES

METRICS = [("deliveryRatio","Delivery Ratio")]
METRICS = [("appBundleReceived:count","Delivered Bundles"),("deliveryRatio","Delivery Ratio"), ("dtnBundleSentToCom:count","Transmitted bundles"),
          ("appBundleReceivedDelay:mean","Mean Delay per Bundle"), ("appBundleReceivedHops:mean","Mean Hops per Bundle"),
          ("sdrBundleStored:timeavg", "Mean Bundles in SDR"), ('EnergyEfficiency', 'Energy Efficiency')]
#METRICS = [('EnergyEfficiency', 'Energy Efficiency')]

OUTPUT_PATH = 'exp-05-10-2019'


ROUTING_ALGORITHMS = [CGR_FA, CGR_2COPIES, CGR_BRUF_POWERED, CGR_MODEL350, CGR_HOPS, BRUF_4, BRUF_3, BRUF_2, BRUF_1, IBRUF_4, IBRUF_3, IBRUF_2, IBRUF_1, SPRAY_AND_WAIT_4, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_2]
COLORS = [ALGORITHM_COLORS[ra] for ra in ROUTING_ALGORITHMS]
LABELS = [ALGORITHM_LABELS[ra] for ra in ROUTING_ALGORITHMS]
STYLES = [ALGORITHM_LINE_STYLES[ra] for ra in ROUTING_ALGORITHMS]
'''
for t in range(0, 24, 6):
    startt = t * 3600;
    endt = (t + 6) * 3600
    slice_dir = os.path.join(OUTPUT_PATH, f'start_t:{startt},end_t:{endt}')
    for metric in METRICS:
        functions = []
        for routing_algotithm in ROUTING_ALGORITHMS:
            ra_dir = os.path.join(slice_dir, routing_algotithm)
            metric_dir = os.path.join(ra_dir, 'metrics')
            print(metric_dir)
            functions.append(getListFromFile(os.path.join(metric_dir, f'METRIC={metric[0]}.txt')))

        plot_name = os.path.join(slice_dir, metric[0])
        if metric[0] == "deliveryRatio" or metric[0] =='EnergyEfficiency' :
            plot_nfunctions(functions, plot_name, LABELS, xlabel='Probability of contact failure', ylabel=metric[1], ftype='png', colors=COLORS, yticks=(0,1.1,.2))
            plot_nfunctions(functions, plot_name, LABELS, xlabel='Probability of contact failure', ylabel=metric[1], ftype='svg', colors=COLORS, yticks=(0,1.1,.2))
        else:
            plot_nfunctions(functions, plot_name, LABELS, xlabel='Probability of contact failure', ylabel=metric[1], ftype='png', colors=COLORS)
            plot_nfunctions(functions, plot_name, LABELS, xlabel='Probability of contact failure', ylabel=metric[1], ftype='svg', colors=COLORS)

'''
PLOT_OUTPUT = os.path.join(OUTPUT_PATH, 'plots'); os.makedirs(PLOT_OUTPUT,exist_ok=True)
for metric in METRICS:
    functions = []
    for routing_algotithm in ROUTING_ALGORITHMS:
        metric_dir = os.path.join(OUTPUT_PATH, 'metrics_av', routing_algotithm)
        print(metric_dir)
        functions.append(getListFromFile(os.path.join(metric_dir, f'METRIC={metric[0]}.txt')))

    plot_name = os.path.join(PLOT_OUTPUT, metric[0])
    if metric[0] == "deliveryRatio" or metric[0] =='EnergyEfficiency' :
        plot_nfunctions(functions, plot_name, LABELS, xlabel='Probability of contact failure', ylabel=metric[1], ftype='png', colors=COLORS, yticks=(0,1.1,.2), styles=STYLES)
        plot_nfunctions(functions, plot_name, LABELS, xlabel='Probability of contact failure', ylabel=metric[1], ftype='svg', colors=COLORS, yticks=(0,1.1,.2), styles=STYLES)
    else:
        plot_nfunctions(functions, plot_name, LABELS, xlabel='Probability of contact failure', ylabel=metric[1], ftype='png', colors=COLORS, styles=STYLES)
        plot_nfunctions(functions, plot_name, LABELS, xlabel='Probability of contact failure', ylabel=metric[1], ftype='svg', colors=COLORS, styles=STYLES)
