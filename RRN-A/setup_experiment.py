from datetime import date
from brufn.experiment_generator import *

DTNSIM_PATH = '/home/fraverta/development/dtnsim/dtnsim/src'
GROUND_TARGETS_EIDS = [x for x in range(7, 32) if x not in [11,17,28]]
SATELLITES = list(range(32, 48))
GS_EID = 1
ROUTING_ALGORITHMS = [CGR_FA, CGR_2COPIES, CGR_BRUF_POWERED, CGR_MODEL350, CGR_HOPS, BRUF_4, BRUF_3, BRUF_2, BRUF_1, IBRUF_4, IBRUF_3, IBRUF_2, IBRUF_1, SPRAY_AND_WAIT_4, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_2]

