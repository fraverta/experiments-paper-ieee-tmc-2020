from brufn.contact_plan import ContactPlan
from brufn.brufspark import BRUFSpark
from brufn.experiment_generator import *
from brufn.helper_ibrufn_function_generator6 import IBRUFNFunctionGenerator
from brufn.utils import print_str_to_file
from brufn.helper_cgr_funciton_generator import OneCopyHelperFunctionGenerator
from datetime import date
import os
import simplejson as json
from pyspark import SparkContext, SparkConf
from brufn.net_metrics_generator import NetMetricGenerator
import simplejson as json
from pprint import pprint

OUTPUT_PATH = 'exp-03-03-2020' #f'exp-{date.today().strftime("%d-%m-%Y")}'  # 'exp-05-10-2019'
CP_PATH = "cp/RingRoad_16sats_Walker_6hotspots_simtime24hs_comrange1000km.txt"
DTNSIM_PATH = '/home/fraverta/development/dtnsim/dtnsim/src'
ALGORITHMS = [CGR_FA, CGR_2COPIES, CGR_BRUF_POWERED, CGR_MODEL350, CGR_HOPS, BRUF_4, BRUF_3, BRUF_2, BRUF_1, IBRUF_4, IBRUF_3, IBRUF_2, IBRUF_1, SPRAY_AND_WAIT_4, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_2]
TS_DURATION = 60
DATA_RATE = 12500
TS_DURATION_IN_SECONDS = 60

GROUND_TARGETS_EIDS = [x for x in range(7, 32) if x not in [11, 17, 28]]
SATELLITES = list(range(32, 48))
GS_EID = 1
F_RENAME = dict([(GROUND_TARGETS_EIDS[i], i + 1) for i in range(len(GROUND_TARGETS_EIDS))]
                + [(SATELLITES[i - len(GROUND_TARGETS_EIDS)], i + 1) for i in range(len(GROUND_TARGETS_EIDS), len(GROUND_TARGETS_EIDS) + len(SATELLITES))]
                + [(GS_EID, len(GROUND_TARGETS_EIDS) + len(SATELLITES) + 1)]
                )


def compute_bruf(algorithm, net, sources, target, f_eids_dtnsim, bruf_dir, rc):
    copies = int(algorithm[algorithm.index('-') + 1:])
    bruf_result_dir_by_target = os.path.join(bruf_dir, 'states_files', f'to-{f_eids_dtnsim[target]}');
    os.makedirs(bruf_result_dir_by_target, exist_ok=True)
    routing_files_path = os.path.join(bruf_dir, 'routing_files');
    os.makedirs(routing_files_path, exist_ok=True)

    f_eids_bruf = dict((k, f_eids_dtnsim[k] - 1) for k in f_eids_dtnsim.keys())  # BRUF IDs start in 0

    print_str_to_file(str(net.generate_statictiscs()), os.path.join(bruf_dir, 'stats.txt'))
    bruf = BRUFSpark(net, [f_eids_bruf[s] for s in sources], f_eids_bruf[target], copies, [i / 100 for i in range(0, 110, 10)], bruf_result_dir_by_target)
    os.makedirs(os.path.join(bruf_dir, 'routing_files'), exist_ok=True)
    conf = SparkConf().setAppName("BRUF-Spark")
    conf = (conf.setMaster('local[6]')
            .set('spark.executor.memory', '3G')
            .set('spark.driver.memory', '12G')
            .set('spark.driver.maxResultSize', '8G'))
    sc = SparkContext(conf=conf)
    bruf.compute_bruf(sc, num_states_per_slice=100, reachability_closure=rc)
    sc.stop()
    bruf.generate_mc_to_dtnsim_all_sources_all_pf(routing_files_path, partially_save_ts_number=50)

    return True


def generate_bruf_function(algorithm, net, sources, target, f_eids_dtnsim, bruf_dir):
    bruf_result_dir = os.path.join(bruf_dir, 'states_files');
    os.makedirs(bruf_result_dir, exist_ok=True)
    routing_files_path = os.path.join(bruf_dir, 'routing_files');
    os.makedirs(routing_files_path, exist_ok=True)
    f_eids_bruf = dict((k, f_eids_dtnsim[k] - 1) for k in f_eids_dtnsim.keys())  # BRUF IDs start in 0

    os.makedirs(os.path.join(bruf_dir, 'routing_files'), exist_ok=True)
    renamed_sources = [f_eids_bruf[s] for s in sources if s != target]
    bruf = BRUFSpark(net, renamed_sources, f_eids_bruf[target], 1, [i / 100 for i in range(0, 110, 10)], bruf_result_dir)
    conf = SparkConf().setAppName("BRUF-Spark")
    conf = (conf.setMaster('local[3]')
            .set('spark.executor.memory', '2G')
            .set('spark.driver.memory', '6G')
            .set('spark.driver.maxResultSize', '4G'))
    # sc = SparkContext(conf=conf)
    # bruf.compute_bruf(sc)

    for pf in [i / 100 for i in range(0, 110, 10)]:
        pf_helper_function_file_path = os.path.join(routing_files_path, f'pf={pf:.2f}');
        os.makedirs(pf_helper_function_file_path, exist_ok=True)
        try:
            helper_function = OneCopyHelperFunctionGenerator(net, f_eids_bruf[target], [n for n in range(net.num_of_nodes) if n != f_eids_bruf[target]], net.num_of_nodes, net.num_of_ts + 1, bruf_result_dir,
                                                             failure_probability=pf).generate()
        except FileNotFoundError:
            print(f"[ERROR] Not solution for {source} to {target} - {bruf_result_dir}")
            exit(1)
        for source in renamed_sources:
            print_str_to_file(json.dumps(helper_function), os.path.join(pf_helper_function_file_path, f"todtnsim-{source}-{f_eids_bruf[target]}-{pf:.2f}.json"))


cp = ContactPlan.parse_contact_plan(CP_PATH)
cp = cp.filter_contact_by_endpoints(GROUND_TARGETS_EIDS + SATELLITES, SATELLITES + [GS_EID])
cp.rename_eids(F_RENAME)

os.makedirs(OUTPUT_PATH, exist_ok=True)
exp_commands = []
for t in range(0, 6, 6):
    startt = t * 3600;
    endt = (t + 6) * 3600
    slice_dir = os.path.join(OUTPUT_PATH, f'start_t:{startt},end_t:{endt}');
    os.makedirs(slice_dir, exist_ok=True)
    sliced_cp_6h = cp.slice_cp(startt, endt)
    sliced_cp_6h = sliced_cp_6h.shift_cp_in_time(-startt)
    sliced_cp_path = os.path.join(slice_dir, f'cp_start_t:{startt},end_t:{endt}.txt')
    sliced_cp_6h.print_to_file(sliced_cp_path)

    net = sliced_cp_6h.generate_slicing_abstraction(TS_DURATION_IN_SECONDS, 30)
    net.print_to_file(slice_dir)
    # NetMetricGenerator(net, range(net.num_of_nodes), [1, 2, 3, 4], [F_RENAME[s] - 1 for s in GROUND_TARGETS_EIDS], slice_dir)
    #reachability_closure = json.load(open(os.path.join(slice_dir, 'transitive_closure.json')),
    #                                 object_hook=lambda d: {int(k): [int(i) for i in v] if isinstance(v, list) else v for k, v in d.items()})

    sliced_cp_from_net = os.path.join(slice_dir, f'FROM-NET-start_t:{startt},end_t:{endt}.txt')
    net.print_dtnsim_cp_to_file(TS_DURATION, DATA_RATE, sliced_cp_from_net)
    print_str_to_file(str(net.generate_statictiscs()), os.path.join(slice_dir, 'new-stat.txt'))

    for algorithm in ALGORITHMS:
        algorithm_dir = os.path.join(slice_dir, algorithm);
        os.makedirs(algorithm_dir, exist_ok=True)
        ini_path = os.path.join(algorithm_dir, 'run.ini')
        is_runnable = True
        frouting_path = None
        if 'BRUF-' in algorithm:
            #is_runnable = compute_bruf(algorithm, net, GROUND_TARGETS_EIDS, GS_EID, F_RENAME, algorithm_dir, reachability_closure)
            frouting_path = 'routing_files/'
        elif CGR_BRUF_POWERED in algorithm:
            #generate_bruf_function(algorithm, net, GROUND_TARGETS_EIDS, GS_EID, F_RENAME, algorithm_dir)
            frouting_path = 'routing_files/'
        
        elif 'NEW_IRUCoPn' in algorithm:
            copies = int(algorithm[algorithm.index('-') + 1:])
            frouting_path = 'routing_files/'
            routing_files_dir = os.path.join(algorithm_dir, frouting_path); os.makedirs(routing_files_dir, exist_ok=True)
		
            func = IBRUFNFunctionGenerator(net, GS_EID, copies, slice_dir, probability_range=[i / 100 for i in range(0, 110, 10)], path_to_load_bruf=slice_dir).generate()
            print_str_to_file(json.dumps(func), os.path.join(routing_files_dir, f'todtnsim-{F_RENAME[GS_EID]}.json'))
            for c in range(1, copies + 1):
                for pf in [i / 100 for i in range(0, 110, 10)]:
                    pf_dir = os.path.join(routing_files_dir, f'pf={pf:.2f}');
                    os.makedirs(pf_dir, exist_ok=True)
                    try:
                        print_str_to_file(json.dumps(func[c][str(pf)]), os.path.join(pf_dir, f'todtnsim-{F_RENAME[GS_EID]-1}-{c}-{pf:.2f}.json'))
                    except BaseException as e:
                        print(f"[Exception] {e}")
        '''
        elif 'IRUCoPn' in algorithm:
            copies = int(algorithm[algorithm.index('-') + 1:])
            frouting_path = 'routing_files/'
            routing_files_dir = os.path.join(algorithm_dir, frouting_path); os.makedirs(routing_files_dir, exist_ok=True)
		
            #func = IBRUFNFunctionGenerator(net, F_RENAME[GS_EID], copies, slice_dir, probability_range=[i / 100 for i in range(0, 110, 10)], path_to_load_bruf=slice_dir).generate()
            #print_str_to_file(json.dumps(func), os.path.join(routing_files_dir, f'todtnsim-{F_RENAME[GS_EID]}.json'))
            #for c in range(1, copies + 1):
                #for pf in [i / 100 for i in range(0, 110, 10)]:
                #    pf_dir = os.path.join(routing_files_dir, f'pf={pf:.2f}');
                #    os.makedirs(pf_dir, exist_ok=True)
                #    try:
                #        print_str_to_file(json.dumps(func[c][str(pf)]), os.path.join(pf_dir, f'todtnsim-{F_RENAME[GS_EID]-1}-{c}-{pf:.2f}.json'))
                #    except BaseException as e:
                #        print(f"[Exception] {e}")
    '''


        if is_runnable:
            traffic = dict((F_RENAME[gt], [F_RENAME[GS_EID]]) for gt in GROUND_TARGETS_EIDS)
            traffic_ttls = dict((F_RENAME[gt], 3600 * 6) for gt in GROUND_TARGETS_EIDS)
            traffic_starts = dict((F_RENAME[gt], {F_RENAME[GS_EID]: 0}) for gt in GROUND_TARGETS_EIDS)
            generate_omnet_ini_file(max(F_RENAME.values()), traffic, algorithm, ini_path, '..' + sliced_cp_from_net[sliced_cp_from_net.rindex('/'):], traffic_startts=traffic_starts, traffic_ttls=traffic_ttls, frouting_path=frouting_path,
                                    ts_duration=TS_DURATION_IN_SECONDS, repeats=NUM_OF_REPS)

            generate_bash_script('run.ini', os.path.join(algorithm_dir, 'run_simulation.sh'), DTNSIM_PATH)
            exp_commands.append(f'echo && echo [Running] {algorithm_dir} && echo '
                                f'&& cd {algorithm_dir} && bash run_simulation.sh && rm results/*.out && cd "$base_dir" '
                                f'&& cd ../../../ '
                                f'&& python -OO parametric_compute_metrics.py {OUTPUT_PATH} start_t:{startt},end_t:{endt} {algorithm} {NUM_OF_REPS} '
                                f'&& bash delete_results.sh {OUTPUT_PATH} {slice_dir[slice_dir.rindex("/") + 1:]} {algorithm}'
                                f'&& cd "$base_dir"'
                                )

with open(os.path.join('run_experiment.sh'), 'w') as f:
    f.write('#!/bin/bash \n')
    f.write(' && \n'.join(exp_commands))

