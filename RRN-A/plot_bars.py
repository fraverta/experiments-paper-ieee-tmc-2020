import os
import sys
from brufn.utils import getListFromFile, plot_nfunctions, prom_llist, print_str_to_file, getListFromFile,plot_bar
from brufn.experiment_generator import *
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from AlgorithmsGraphStyles import ALGORITHM_LABELS, ALGORITHM_COLORS, ALGORITHM_LINE_STYLES

def compute_f_diference(f, f_reference):
    if len(f) != len(f_reference):
        print(f)
        print(f_reference)
        raise ValueError("[Error] compute_f_diference(f, f_reference): f and f_reference must have the same lenght")
    return [(f_reference[i][0], f[i][1] - f_reference[i][1]) for i in range(len(f))]


METRICS = [("deliveryRatio","Delivery Ratio"), ('EnergyEfficiency', 'Energy Efficiency'),("appBundleReceivedDelay:mean","Mean Delay per Bundle")]
REFERENCE_ALGORITHM = CGR_MODEL350

ROUTING_ALGORITHMS = [CGR_FA, CGR_2COPIES, CGR_BRUF_POWERED, CGR_MODEL350, CGR_HOPS, BRUF_4, BRUF_3, BRUF_2, BRUF_1, IBRUF_4, IBRUF_3, IBRUF_2, IBRUF_1, SPRAY_AND_WAIT_4, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_2]
COLORS = [ALGORITHM_COLORS[ra] for ra in ROUTING_ALGORITHMS]
LABELS = [ALGORITHM_LABELS[ra] for ra in ROUTING_ALGORITHMS]


OUTPUT_PATH = 'exp-05-10-2019'

for t in range(0, 24, 6):
    startt = t * 3600;
    endt = (t + 6) * 3600
    slice_dir = os.path.join(OUTPUT_PATH, f'start_t:{startt},end_t:{endt}')
    graps = []
    for metric in METRICS:
        plots_output_dir = os.path.join(slice_dir, 'plots-bar')
        os.makedirs(plots_output_dir, exist_ok=True)
        f_reference = getListFromFile(os.path.join(slice_dir, REFERENCE_ALGORITHM, 'metrics', f'METRIC={metric[0]}.txt'))[1:-1]
        f_metric_av_by_algorithm = []
        for routing_algorithm in ROUTING_ALGORITHMS:
            f_metric_by_net = []
            ra_dir = os.path.join(slice_dir, routing_algorithm, 'metrics')
            print(f"{ra_dir}")
            f_metric_av = getListFromFile(os.path.join(ra_dir, f'METRIC={metric[0]}.txt'))[1:-1]
            if metric in [("deliveryRatio","Delivery Ratio"), ('EnergyEfficiency', 'Energy Efficiency')]:
                f_metric_av_by_algorithm.append([(x, y*100) for x,y in compute_f_diference(f_metric_av, f_reference)])
            elif metric == ("appBundleReceivedDelay:mean","Mean Delay per Bundle"):
                f_metric_av_by_algorithm.append([(f_reference[i][0], 100 - (f_metric_av[i][1] * 100 / f_reference[i][1])) if f_reference[i][1] != 0 else (0, 0) for i in range(len(f_reference))])

        plot_name = os.path.join(plots_output_dir, metric[0])
        plot_bar(f_metric_av_by_algorithm, plot_name, LABELS, xlabel='Probability of contact failure', ylabel='% Improvement regarding CGR', ftype='png', colors=COLORS, bar_width=0.3, separation_width=0.4)
        plot_bar(f_metric_av_by_algorithm, plot_name, LABELS, xlabel='Probability of contact failure', ylabel='% Improvement regarding CGR', ftype='svg', colors=COLORS, bar_width=0.3, separation_width=0.4)


graphs = []
plots_output_dir = os.path.join(OUTPUT_PATH, 'plots-bar'); os.makedirs(plots_output_dir, exist_ok=True)
for metric in METRICS:
    f_reference = getListFromFile(os.path.join(OUTPUT_PATH,'metrics_av', REFERENCE_ALGORITHM, f'METRIC={metric[0]}.txt'))[1:-1]
    f_metric_av_by_algorithm = []
    for routing_algorithm in ROUTING_ALGORITHMS:
        f_metric_by_net = []
        ra_dir = os.path.join(OUTPUT_PATH,'metrics_av', routing_algorithm)
        print(f"{ra_dir}")
        f_metric_av = getListFromFile(os.path.join(ra_dir, f'METRIC={metric[0]}.txt'))[1:-1]
        if metric in [("deliveryRatio","Delivery Ratio"), ('EnergyEfficiency', 'Energy Efficiency')]:
            f_metric_av_by_algorithm.append([(x, y*100) for x,y in compute_f_diference(f_metric_av, f_reference)])
        elif metric == ("appBundleReceivedDelay:mean","Mean Delay per Bundle"):
            f_metric_av_by_algorithm.append([(f_reference[i][0], 100 - (f_metric_av[i][1] * 100 / f_reference[i][1])) if f_reference[i][1] != 0 else (0, 0) for i in range(len(f_reference))])

    plot_name = os.path.join(plots_output_dir, metric[0])
    plot_bar(f_metric_av_by_algorithm, plot_name, LABELS, xlabel='Probability of contact failure', ylabel='% Improvement regarding CGR', ftype='png', colors=COLORS, bar_width=0.3, separation_width=0.4)
    plot_bar(f_metric_av_by_algorithm, plot_name, LABELS, xlabel='Probability of contact failure', ylabel='% Improvement regarding CGR', ftype='svg', colors=COLORS, bar_width=0.3, separation_width=0.4)
    graphs.append(plot_name + '.svg')
