# A Markov Decision Process for Routing in Space DTNs with Uncertain Contact Plans

Given 

 - Contact Plan  annotated with links failure probability
 - A list of traffic, each one denoted by its from and target node identifiers
   and a creation time stamp.

These scripts compute the routing decisions which maximizes the overall traffic delivery probability.


