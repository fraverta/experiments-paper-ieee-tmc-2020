from svg_stack import *


def arrange_into_table(rows, cols, spacing_x, spacing_y, fnames):
    doc = Document()
    layout = VBoxLayout()
    for i in range(rows):
        layout_row = HBoxLayout()
        for j in range(cols):
            fname = fnames[i * cols + j]
            layout_row.addSVG(fname, alignment=AlignCenter)

        layout_row.setSpacing(spacing_x)
        layout.addLayout(layout_row)
        layout.setSpacing(spacing_y)

    doc.setLayout(layout)
    return doc

def combine_into_table_and_save(rows, cols, spacing_x, spacing_y, fnames, fname):
    doc = arrange_into_table(rows, cols, spacing_x, spacing_y, fnames)
    with open(fname, 'w') as f:
        doc.save(f)

def main():
    usage = '''%prog FILE1 [FILE2] [...] [options]

This will concatenate FILE1, FILE2, ... to a new svg file. Also, this will arrange 
the provided elements into a table which given number of rows and columns. 
The table will be populated by rows.

EXAMPLE

To layout 4 svg files (f1,f2,f3 and f4) into a table of 2 rows and 2 cols and save it to ft.svg use:
  %prog --rows=2 --cols=2 f1.svg f2.svg f3.svg f4.svg > ft.svg

'''

    parser = OptionParser(usage, version=VERSION)
    parser.add_option("--rows",type='int',
                      help='number of grid rows (required)')
    parser.add_option("--cols",type='int',
                      help='number of grid cols (required)')
    parser.add_option("--spacing_x",type='str', help='size of margin (in any units, px default)')
    parser.add_option("--spacing_y",type='str', default='0px', help='size of margin (in any units, px default)')
    (options, args) = parser.parse_args()
    fnames = args

    if options.spacing_x is not None:
        spacing_x = convert_to_pixels(*get_unit_attr(options.spacing_x))
    else:
        spacing_x = 0

    if options.spacing_y is not None:
        spacing_y = convert_to_pixels(*get_unit_attr(options.spacing_y))
    else:
        spacing_y = 0

    if options.rows is None:
        raise ValueError('You must provide the number of rows that you want in the table')

    if options.cols is None:
        raise ValueError('You must provide the number of cols that you want in the table')

    if len(fnames) != options.rows * options.cols:
        raise ValueError('You must supply as much files as it requires to populate the table')

    doc = arrange_into_table(options.rows, options.cols, spacing_x, spacing_y, fnames)
    if 0:
        fd = open('tmp.svg',mode='wb')
    else:
        fd = sys.stdout
    doc.save(fd)
    

if __name__=='__main__':
    main()

