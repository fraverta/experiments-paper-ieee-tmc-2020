from setup_experiment import INPUT_CP, HOT_SPOTS_EIDS, COLD_SPOT_EIDS, OUTPUT_PATH, NUM_OF_REPS, SATELLITE_EIDS, DTNSIM_PATH, ROUTING_ALGORITHMS, HOURS_RANGE, TS_DURATION_IN_SECONDS
from brufn.contact_plan import ContactPlan
from brufn.experiment_generator import generate_omnet_ini_file, generate_omnet_traffic, generate_bash_script, CGR_BRUF_POWERED
from brufn.brufspark import BRUFSpark
from brufn.utils import print_str_to_file
from typing import *
import os
from brufn.helper_cgr_funciton_generator import OneCopyHelperFunctionGenerator
import simplejson as json
from pyspark import SparkContext, SparkConf
from brufn.net_metrics_generator import NetMetricGenerator
from brufn.helper_ibrufn_function_generator6 import IBRUFNFunctionGenerator
from brufn.experiment_generator import *

OUTPUT_PATH = 'exp-03-07-2020'
ROUTING_ALGORITHMS = [CGR_FA, CGR_2COPIES, CGR_BRUF_POWERED, CGR_MODEL350, CGR_HOPS, BRUF_4, BRUF_3, BRUF_2, BRUF_1, IBRUF_4, IBRUF_3, IBRUF_2, IBRUF_1, SPRAY_AND_WAIT_4, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_2]

def compute_bruf(algorithm, net, ttl, source, target, f_eids_dtnsim, bruf_dir, rc) -> bool:
    copies = int(algorithm[algorithm.index('-') + 1:])
    bruf_result_dir = os.path.join(bruf_dir, 'states_files'); os.makedirs(bruf_result_dir, exist_ok=True)
    routing_files_path = os.path.join(bruf_dir, 'routing_files'); os.makedirs(routing_files_path, exist_ok=True)

    f_eids_bruf = dict((k, f_eids_dtnsim[k] - 1) for k in f_eids_dtnsim.keys())  # BRUF IDs start in 0
    bruf = BRUFSpark(net, [f_eids_bruf[source]], f_eids_bruf[target], copies, [i / 100 for i in range(0, 110, 10)], bruf_result_dir)
    if f_eids_bruf[target] in range(net.num_of_nodes):
        conf = SparkConf().setAppName("BRUF-Spark")
        conf = (conf.setMaster('local[2]')
            .set('spark.executor.memory', '3G')
            .set('spark.driver.memory', '8G')
            .set('spark.driver.maxResultSize', '2G'))
        sc = SparkContext(conf=conf)
        bruf.compute_bruf(sc, num_states_per_slice=100, reachability_closure=rc)
        sc.stop()

    os.makedirs(os.path.join(bruf_dir, 'routing_files'), exist_ok=True)
    bruf.generate_mc_to_dtnsim_all_sources_all_pf(routing_files_path, partially_save_ts_number=50)

    return True

def generate_bruf_function(algorithm, net, sources, target, f_eids_dtnsim, bruf_dir):
    bruf_result_dir = os.path.join(bruf_dir, 'states_files'); os.makedirs(bruf_result_dir, exist_ok=True)
    routing_files_path = os.path.join(bruf_dir, 'routing_files'); os.makedirs(routing_files_path, exist_ok=True)
    f_eids_bruf = dict((k, f_eids_dtnsim[k] - 1) for k in f_eids_dtnsim.keys())  # BRUF IDs start in 0
    
    os.makedirs(os.path.join(bruf_dir, 'routing_files'), exist_ok=True)
    renamed_sources = [f_eids_bruf[s] for s in sources if s != target]
    bruf = BRUFSpark(net, renamed_sources, f_eids_bruf[target], 1, [i / 100 for i in range(0, 110, 10)], bruf_result_dir)
    conf = SparkConf().setAppName("BRUF-Spark")
    conf = (conf.setMaster('local[6]')
        .set('spark.executor.memory', '3G')
        .set('spark.driver.memory', '12G')
        .set('spark.driver.maxResultSize', '8G'))
    sc = SparkContext(conf=conf)
    bruf.compute_bruf(sc, num_states_per_slice=100)
    sc.stop()

    for pf in [i / 100 for i in range(0, 110, 10)]:
        pf_helper_function_file_path = os.path.join(routing_files_path, f'pf={pf:.2f}'); os.makedirs(pf_helper_function_file_path, exist_ok=True)
        try:
            helper_function = OneCopyHelperFunctionGenerator(f_eids_bruf[target], [n for n in range(net.num_of_nodes) if n != f_eids_bruf[target]], net.num_of_nodes, net.num_of_ts + 1, bruf_result_dir, failure_probability=pf).generate()
        except FileNotFoundError:
            print(f"[ERROR] Not solution for {source} to {target} - {bruf_result_dir}")
            exit(1)

        for source in renamed_sources:
            print_str_to_file(json.dumps(helper_function), os.path.join(pf_helper_function_file_path, f"todtnsim-{source}-{f_eids_bruf[target]}-{pf:.2f}.json"))
            #FileExistsError('Just to jump to exception')


def main():
    for target in COLD_SPOT_EIDS:
        exp_commands = ['source  ../../../venv/bin/activate', 'base_dir="$PWD"']
        target_dir = os.path.join(OUTPUT_PATH, f'Target-{target}')
        for ttl in HOURS_RANGE:
            ttl_dir = os.path.join(target_dir, f'ttl-{ttl}')
            os.makedirs(ttl_dir, exist_ok=True)

            #CP preparation
            f_eids_dtnsim = dict([(HOT_SPOTS_EIDS[0], 1), (HOT_SPOTS_EIDS[1], 1), (target, len(SATELLITE_EIDS) + 2)] + [(sat, id + 2) for id, sat in  enumerate(SATELLITE_EIDS)])
            cp = ContactPlan.parse_contact_plan(INPUT_CP)
            cp = cp.filter_contact_by_endpoints(HOT_SPOTS_EIDS + SATELLITE_EIDS, SATELLITE_EIDS + [target])
            cp = cp.slice_cp(0, ttl)
            cp.rename_eids(f_eids_dtnsim, allow_multiple_same_id=True); print_str_to_file(str(f_eids_dtnsim), os.path.join(ttl_dir,'f_rename'))

            net = cp.generate_slicing_abstraction(TS_DURATION_IN_SECONDS, TS_DURATION_IN_SECONDS/2)
            net.print_to_file(ttl_dir)
            print_str_to_file(str(net.generate_statictiscs()), os.path.join(ttl_dir, 'net_abstraction_stat.txt'))
            ttl_cp_path = os.path.join(ttl_dir, 'FROM-NET-cp.txt'); net.print_dtnsim_cp_to_file(60, 12000, ttl_cp_path)
            #NetMetricGenerator(net, range(net.num_of_nodes), [1, 2, 3, 4], [1], ttl_dir)        
            #reachability_closure = json.load(open(os.path.join(ttl_dir,'transitive_closure.json')),
            #       object_hook=lambda d: {int(k): [int(i) for i in v] if isinstance(v, list) else v for k, v in d.items()})

            for algorithm in ROUTING_ALGORITHMS:
                algorithm_dir = os.path.join(ttl_dir, algorithm); os.makedirs(algorithm_dir, exist_ok=True)
                ini_path = os.path.join(algorithm_dir, 'run.ini')
                print(f"[Running] {algorithm_dir}")

                is_runnable = True
                frouting_path = None
                if 'BRUF-' in algorithm:                    
                    #is_runnable = compute_bruf(algorithm, net, ttl, 1, target, f_eids_dtnsim, algorithm_dir, reachability_closure)
                    frouting_path = 'routing_files/'
                elif CGR_BRUF_POWERED in algorithm:
                    generate_bruf_function(algorithm, net, [1], target, f_eids_dtnsim, algorithm_dir)
                    frouting_path = 'routing_files/'

                elif 'IRUCoPn' in algorithm:
                    copies = int(algorithm[algorithm.index('-') + 1:])
                    frouting_path = 'routing_files/'
                    routing_files_dir = os.path.join(algorithm_dir, frouting_path);
                    os.makedirs(routing_files_dir, exist_ok=True)
                    '''
                    func = IBRUFNFunctionGenerator(net, f_eids_dtnsim[target], copies, ttl_dir, probability_range=[i / 100 for i in range(0, 110, 10)], path_to_load_bruf=ttl_dir).generate()
                    print_str_to_file(json.dumps(func), os.path.join(routing_files_dir, f'todtnsim-{f_eids_dtnsim[target]}.json'))
                    for c in range(1, copies + 1):
                        for pf in [i / 100 for i in range(0, 110, 10)]:
                            pf_dir = os.path.join(routing_files_dir, f'pf={pf:.2f}');
                            os.makedirs(pf_dir, exist_ok=True)
                            try:
                                print_str_to_file(json.dumps(func[c][str(pf)]), os.path.join(pf_dir, f'todtnsim-{f_eids_dtnsim[target]-1}-{c}-{pf:.2f}.json'))
                            except BaseException as e:
                                print(f"[Exception] {e}")
                    '''
                if is_runnable:
                    traffic = {1: [f_eids_dtnsim[target]]}; traffic_ttls = {1: ttl};
                    generate_omnet_ini_file(len(SATELLITE_EIDS) + 2, traffic, algorithm, ini_path, os.path.relpath(ttl_cp_path, algorithm_dir), frouting_path=frouting_path, repeats=NUM_OF_REPS, traffic_ttls=traffic_ttls, ts_duration=TS_DURATION_IN_SECONDS)
                    generate_bash_script('run.ini', os.path.join(algorithm_dir,'run_simulation.sh'), DTNSIM_PATH)
                    exp_commands.append(f'echo && echo [Running] {algorithm_dir} && echo '
                                        f'&& cd {algorithm_dir[ttl_dir.rindex("/") + 1:]} && bash run_simulation.sh && rm results/*.out && cd "$base_dir" '
                                        f'&& cd ../.. '                                        
                                        f'&& python -OO parametric_compute_metrics.py {OUTPUT_PATH} {target} {ttl} {algorithm} {NUM_OF_REPS} ' 
                                        f'&& bash delete_results_by_ttl.sh {OUTPUT_PATH} {target} {ttl} {algorithm}'
                                        f'&& cd "$base_dir"'
                                    )

            with open(os.path.join(target_dir, 'run_experiment.sh'), 'w') as f:
                f.write('#!/bin/bash \n')
                f.write(' && \n'.join(exp_commands))

main()
