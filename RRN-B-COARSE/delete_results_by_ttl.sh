#!/bin/bash 

if [ $# -eq 4 ]
then
    EXP_PATH=$1
    TARGET=$2
    TTL=$3
    ALGORITHM=$4

    echo $EXP_PATH/Target-${TARGET}/ttl-${TTL}/${ALGORITHM}/results
    rm -r $EXP_PATH/Target-${TARGET}/ttl-${TTL}/${ALGORITHM}/results

else
    echo $#
    echo "[Error] delete_results_by_ttl: You must put delete_results_by_ttl"
fi
