import os
import sys
from brufn.utils import getListFromFile, plot_nfunctions
from setup_experiment import HOT_SPOTS_EIDS, COLD_SPOT_EIDS, OUTPUT_PATH, ROUTING_ALGORITHMS, HOURS_RANGE
from svgtogrid import combine_into_table_and_save
from brufn.experiment_generator import *
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from AlgorithmsGraphStyles import ALGORITHM_LABELS, ALGORITHM_COLORS, ALGORITHM_LINE_STYLES

METRICS = [("deliveryRatio","Delivery Ratio")]
METRICS = [("appBundleReceived:count","Delivered Bundles"),("deliveryRatio","Delivery Ratio"), ("dtnBundleSentToCom:count","Transmitted bundles"),
          ("appBundleReceivedDelay:mean","Mean Delay per Bundle"), ("appBundleReceivedHops:mean","Mean Hops per Bundle"),
          ("sdrBundleStored:timeavg", "Mean Bundles in SDR"), ('EnergyEfficiency', 'Energy Efficiency')]
#METRICS = [('EnergyEfficiency', 'Energy Efficiency')]

OUTPUT_PATH = 'exp-03-07-2020'
HOT_SPOTS_EIDS = [1, 9]
COLD_SPOT_EIDS = [11] #[11, 50, 110]

ROUTING_ALGORITHMS = [CGR_FA, CGR_2COPIES, CGR_BRUF_POWERED, CGR_MODEL350, CGR_HOPS, BRUF_4, BRUF_3, BRUF_2, BRUF_1, IBRUF_4, IBRUF_3, IBRUF_2, IBRUF_1, SPRAY_AND_WAIT_4, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_2]
COLORS = [ALGORITHM_COLORS[ra] for ra in ROUTING_ALGORITHMS]
LABELS = [ALGORITHM_LABELS[ra] for ra in ROUTING_ALGORITHMS]

for target in COLD_SPOT_EIDS:
    target_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), OUTPUT_PATH, f'Target-{target}')
    for metric in METRICS:
        graphs = []
        for ttl in [24 * 3600]: #[h*3600 for h in range(2,26,2)]:
            ttl_dir = os.path.join(target_dir, f'ttl-{ttl}')
            functions = []
            for routing_algotithm in ROUTING_ALGORITHMS:
                ra_dir = os.path.join(ttl_dir, routing_algotithm)
                metric_dir = os.path.join(ra_dir, 'metrics')
                print(metric_dir)
                functions.append(getListFromFile(os.path.join(metric_dir, f'METRIC={metric[0]}.txt')))

            plot_name = os.path.join(ttl_dir,metric[0])
            if metric[0] == "deliveryRatio" or metric[0] =='EnergyEfficiency' :
                plot_nfunctions(functions, plot_name, LABELS, xlabel='Probability of contact failure', ylabel=metric[1], ftype='png', colors=COLORS, yticks=(0,1.1,.2))
                plot_nfunctions(functions, plot_name, LABELS, xlabel='Probability of contact failure', ylabel=metric[1], ftype='svg', colors=COLORS, yticks=(0,1.1,.2))
            else:
                plot_nfunctions(functions, plot_name, LABELS, xlabel='Probability of contact failure', ylabel=metric[1], ftype='png', colors=COLORS)
                plot_nfunctions(functions, plot_name, LABELS, xlabel='Probability of contact failure', ylabel=metric[1], ftype='svg', colors=COLORS)

            graphs.append(plot_name + '.svg')
       
        join_output_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), OUTPUT_PATH, 'joinplots')
        os.makedirs(join_output_path, exist_ok=True)
        fname = os.path.join(join_output_path, f'target:{target}-{metric[0]}-WITH-BRUF-SIMS')
        combine_into_table_and_save(len(graphs) // 6, 6, 0, 0, graphs, fname + '.svg')
        #combine_into_table_and_save(len(graphs)//6, 6, 0, 0, graphs, fname + '.svg')
        #os.system('inkscape %s &'% (fname + '.svg') )
        os.system('inkscape --export-png=%s %s'%(fname + '.png', fname + '.svg'))
