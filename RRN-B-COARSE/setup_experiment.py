from datetime import date
from brufn.experiment_generator import BRUF_1, BRUF_2, BRUF_3, BRUF_4, CGR_MODEL350, CGR_FA, SPRAY_AND_WAIT_2, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_4, CGR_BRUF_POWERED

DTNSIM_PATH = '/home/fraverta/development/dtnsim/dtnsim/src'
INPUT_CP = 'scenario/walker-12leo-10gs-100gt-24hrs-contact-plan.txt'
ROUTING_ALGORITHMS = [] #[BRUF_1, BRUF_2, BRUF_3, BRUF_4, CGR_FA, CGR_MODEL350, SPRAY_AND_WAIT_2, SPRAY_AND_WAIT_3, SPRAY_AND_WAIT_4, CGR_BRUF_POWERED] #[BRUF_1, BRUF_2, BRUF_3, BRUF_4, SPRAY_AND_WAIT_4]

COLD_SPOT_EIDS = [11]
HOT_SPOTS_EIDS = [1, 9]
SATELLITE_EIDS = list(range(111, 123))
NUM_OF_REPS = 1250
HOURS_RANGE = [24*3600] #[3600 * h for h in range(2, 25, 2)]
OUTPUT_PATH = f'exp-{date.today().strftime("%d-%m-%Y")}'
